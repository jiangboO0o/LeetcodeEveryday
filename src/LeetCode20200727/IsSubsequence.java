package LeetCode20200727;

/**
 * @program: LeetCodeEveryDay
 * @description: 判断子序列
 * @author: LongJiangBo
 * @create: 2020-07-27 22:26
 **/
public class IsSubsequence {
    public boolean isSubsequence(String s,String t){
        int n = s.length(), m = t.length();
        int i = 0, j = 0;
        while (i < n && j < m){
            if (s.charAt(i) == t.charAt(j)){
                i++;
            }
            j++;
        }
        return i == n;
    }
}
