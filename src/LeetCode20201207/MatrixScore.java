package LeetCode20201207;

/**
 * @program: LeetcodeEveryday
 * @description:
 * @author: longjiangbo
 * @create: 2020-12-08 11:14
 **/
public class MatrixScore {

    public static void main(String[] args) {
        int[][] A = {{0,0,1,1},{1,0,1,0},{1,1,0,0}};
        int num = 0;
        int m = A.length;
        int n = A[0].length;
        //如果每行第一个元素不为1，则反转此行
        for(int i = 0; i < m; i++){
            if(A[i][0] != 1){
                for(int j = 0; j < n; j++){
                    num = 1 - A[i][j];
                    A[i][j] = num;
                }
            }
        }
        //统计每列的0和1的个数，如果0的数量大于1，则反转
        for(int j = 0; j < n; j++){
            int num0 = 0;
            int num1 = 0;
            for(int i = 0; i < m; i++){
                if(A[i][j] == 0){
                    num0++;
                }else{
                    num1++;
                }
            }
            if(num0 > num1){
                for(int i = 0; i < m; i++){
                    num = 1 - A[i][j];
                    A[i][j] = num;
                }
            }
        }
        //求和
        int result = 0;
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(A[i][j] != 0){
                    result += 1<<(n-j-1);
                }
            }
        }
        System.out.println(result);
    }
}
