package Leetcode20200716;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * @program: LeetCodeEveryDay
 * @description: 785判断二分图(主要考察图的深度优先搜索)
 * @author: LongJiangBo
 * @create: 2020-07-16 20:59
 **/
public class IsGraphBipartite {
    private static final int UNCOLORED = 0;
    private static final int RED = 1;
    private static final int GREEN = 2;
    private int[] color;
    private boolean valid;

    public boolean isBipartite(int[][] graph){
        int n = graph.length;
        valid = true;
        color = new int[n];
        //给指定的数组填充指定的元素
        Arrays.fill(color,UNCOLORED);
        for (int i = 0; i < n && valid; ++i){
            if (color[i] == UNCOLORED){
                dfs(i,RED,graph);
            }
        }
        return valid;
    }

    private void dfs(int node, int c, int[][] graph) {
        color[node] = c;
        int cNei = c == RED ? GREEN : RED;
        for (int neighbor : graph[node]){
            if (color[neighbor] == UNCOLORED){
                dfs(neighbor,cNei,graph);
                if (!valid){
                    return;
                }
            }else if (color[neighbor] != cNei){
                valid = false;
                return;
            }
        }
    }

}
